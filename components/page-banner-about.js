import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const PageBannerAbout = ({ title, name }) => {
  return (
    <section className="pageBannerAbout">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="container-about-position">
              <div className="title-about-position">
                <h2 className="sec_title pb-3">
                  So<span>bre N</span>ós
                </h2>
                <Col lg={10} md={12} className="text-about-content">
                  <p>
                    Nascemos como uma editora em 1999 para apoiar a Associação
                    Vitória em Cristo (AVEC) com materiais evangelísticos. Com o
                    crescimento exponencial, surgiram novas frontes, como a
                    Central Gospel Music. Posteriormente, avançamos para o
                    Empreendedorismo e Ensino. Nos tornamos, portanto, uma rede
                    central que leva a Palavra de Deus através de livros,
                    canções, negócios, artes e estudos.
                  </p>
                  <p>
                    <span className="font-weight-bold">Missão</span> — Juntos,
                    vamos apresentar o conhecimento da verdade, apresentando-o
                    em todos os formatos e de todas as formas, com o objetivo de
                    mudar a vida das pessoas e, por conseguinte, da sociedade.
                  </p>
                  <p>
                    <span className="font-weight-bold">Visão</span> — Empreender
                    com resiliência. Sonhar com sabedoria. Expressar arte com
                    valor. Ensinar com determinação. Família, oficio, saúde e
                    propósito. A partir da plenitude e da excelência, nós
                    desenvolvemos pessoas e formamos líderes.
                  </p>
                  <p>
                    <span className="font-weight-bold">Valores</span> — Fé.
                    Resiliência. Esforço. Plenitude. Oportunidades. Excelência.
                    Criatividade. Transformação. Crescimento.
                  </p>
                </Col>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PageBannerAbout;

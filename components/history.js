import React from "react";
import { ImageHistory } from "@/data";
import { Col, Container, Row } from "react-bootstrap";

const History = () => {
  const { history } = ImageHistory;
  return (
    <section className="commonSection ab_agency">
      <Container>
        <Row>
          <Col lg={12} md={6} sm={12} className="PR_79 text-center">
            <h2 className="sec_title_contact mb-3 text-center">
              Nos<span>sa Hist</span>ória
            </h2>
            <h5 className="text-center subtitle-about mb-5 font-weight-bold">
              Um pouco da nossa trajetória <br /> de 21 anos no mercado.
            </h5>
            <img
              src={history}
              alt="nossa história"
              className="img-fluid m20"
              width="650"
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default History;

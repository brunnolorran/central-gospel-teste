import React from "react";

const GoogleMap = ({ extraClass }) => {
  return (
    <div className={`google-map__${extraClass}`}>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3674.3686605004223!2d-43.3848243850337!3d-22.93664618499664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9bd91ea7dff0a9%3A0xd1f7cac66a287959!2sEstrada%20do%20Guerengu%C3%AA%2C%201851%20-%20Taquara%2C%20Rio%20de%20Janeiro%20-%20RJ%2C%2022713-002!5e0!3m2!1spt-BR!2sbr!4v1617402746338!5m2!1spt-BR!2sbr"
        width="800"
        height="600"
        allowfullscreen=""
        loading="lazy"
      ></iframe>
    </div>
  );
};

export default GoogleMap;

import React from "react";
import Footer from "@/components/footer2";
import Layout from "@/components/layout";
import GoogleMap from "@/components/google-map";
import ContactForm from "@/components/contact-form";
import PageBannerContact from "@/components/page-banner-contact";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import HeaderOne from "@/components/header-one";
const ContactPage = () => {
  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout PageTitle="Contato">
          <HeaderOne />
          <PageBannerContact />
          <ContactForm />
          <GoogleMap extraClass="contact-page" />
          <Footer />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export default ContactPage;
